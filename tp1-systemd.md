# TP1 Systemd
## I `systemd`-basics
### 1. First steps
* s'assurer que `systemd` est PID1
```bash
$ ps -ef | head -n2
   UID          PID    PPID  C STIME TTY          TIME CMD
   root           1       0  0 14:15 ?        00:00:01 /usr/lib/systemd/systemd --switched-root --system --deserialize 30
```
* lister les process non kernel
```bash
$ ps -U root -u root --deselect
   PID TTY          TIME CMD
   666 ?        00:00:00 chronyd
   670 ?        00:00:00 dbus-broker-lau
   671 ?        00:00:00 dbus-broker
   964 ?        00:00:00 systemd
   966 ?        00:00:00 (sd-pam)
   971 ?        00:00:00 sshd
   972 pts/0    00:00:00 bash
   1032 pts/0    00:00:00 ps
```
__chronyd__ : daemon remplaçant ntpd dans l'implémentation du Network Time Protocole.

__dbus-broker-lau__ & __dbus-broker__ : process du daemon D-Bus servant à la communication entre les processus

__systemd__ : instance user du daemon sytemd

__sd-pam__ : 

__sshd__ : daemon ssh

__bash__ : shell depuis lequel la commande à été lancé

__ps__ : commande permettant de lister les process

### 2. Gestion du temps
* UTC correspond à l'heure de référence universel. Local Time et l'heure de la machine suivent sont fuseau horaire (ici CET : Central European Time). RTC correspond à une heure calculer en interne par la machine à l'aide d'une horloge.

* Changer de timezone
```bash
$ sudo timedatectl set-timezone "Europe/Dublin"
$ timedatectl 
   Local time: Tue 2019-12-03 14:00:33 GMT
   Universal time: Tue 2019-12-03 14:00:33 UTC
   RTC time: Tue 2019-12-03 15:00:29
   Time zone: Europe/Dublin (GMT, +0000)
   System clock synchronized: yes
   NTP service: active
   RTC in local TZ: no
```

* Désactiver la sychronysation NTP
```bash
$ sudo timedatectl set-ntp false
$ timedatectl 
   Local time: Tue 2019-12-03 15:07:20 CET
   Universal time: Tue 2019-12-03 14:07:20 UTC
   RTC time: Tue 2019-12-03 15:07:16
   Time zone: Europe/Paris (CET, +0100)
   System clock synchronized: yes
   NTP service: inactive
   RTC in local TZ: no
```

### 3. Gestion de nom
Les différents types de nom :
* static : hostname standard, stocké dans `/etc/hostname`
* pretty : hostname libre, utilisé uniquement pour l'utilisateur
* trensient : hostname pouvant être configuré via le DHCP
Sur une machine de prod le mieux est d'utiliser le static qui est figé dans la machine.

### 4. Gestion du réseau (et résolution de noms)

#### NetworkManager
afficher les informations DHCP récupérées par NetworkManager (sur une interface en DHCP)
```bash
$ nmcli con show enp0s9 | grep DHCP | less
   DHCP4.OPTION[1]:                        dhcp_lease_time = 1200
   DHCP4.OPTION[2]:                        dhcp_rebinding_time = 1050
   DHCP4.OPTION[3]:                        dhcp_renewal_time = 600
   DHCP4.OPTION[4]:                        dhcp_server_identifier = 192.168.100.100
   DHCP4.OPTION[5]:                        expiry = 1575387333
   DHCP4.OPTION[6]:                        ip_address = 192.168.100.101
   DHCP4.OPTION[7]:                        requested_broadcast_address = 1
   DHCP4.OPTION[8]:                        requested_dhcp_server_identifier = 1
   DHCP4.OPTION[9]:                        requested_domain_name = 1
   DHCP4.OPTION[10]:                       requested_domain_name_servers = 1
   DHCP4.OPTION[11]:                       requested_domain_search = 1
   DHCP4.OPTION[12]:                       requested_host_name = 1
   DHCP4.OPTION[13]:                       requested_interface_mtu = 1
   DHCP4.OPTION[14]:                       requested_ms_classless_static_routes = 1
[...]
```

#### `systemd-networkd`

* Stopper et désactiver NetworkManager
```bash
$ sudo systemctl stop NetworkManager
$ sudo systemctl disable NetworkManager
   Removed /etc/systemd/system/multi-user.target.wants/NetworkManager.service.
   Removed /etc/systemd/system/dbus-org.freedesktop.nm-dispatcher.service.
   Removed /etc/systemd/system/network-online.target.wants/NetworkManager-wait-online.service.
```

* Démarrer et Activer `systemd-networkd`
```bash
$ sudo systemctl start systemd-networkd
$ sudo systemctl enable systemd-networkd
   Created symlink /etc/systemd/system/dbus-org.freedesktop.network1.service → /usr/lib/systemd/system/systemd-networkd.service.
   Created symlink /etc/systemd/system/multi-user.target.wants/systemd-networkd.service → /usr/lib/systemd/system/systemd-networkd.service.
   Created symlink /etc/systemd/system/sockets.target.wants/systemd-networkd.socket → /usr/lib/systemd/system/systemd-networkd.socket.
   Created symlink /etc/systemd/system/network-online.target.wants/systemd-networkd-wait-online.service → /usr/lib/systemd/system/systemd-networkd-wait-online.service.

```

* Editer la configuration d'une carte réseau
```bash
$ cat /etc/systemd/network/priv.network 
   [Match]
   Name=enp0s9

   [Network]
   Address=192.168.100.11/24

$ ip a
[...]
   3: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
      link/ether 08:00:27:cb:24:8a brd ff:ff:ff:ff:ff:ff
      inet 192.168.100.11/24 brd 192.168.11.255 scope global enp0s8
         valid_lft forever preferred_lft forever
      inet6 fe80::af2f:93c3:3001:53f0/64 scope link noprefixroute 
         valid_lft forever preferred_lft forever
[...]
```

#### `systemd-resolvd`

* activer la résolution de noms par `systemd-resolved `
```bash
$ sudo systemctl start systemd-resolved
$ sudo systemctl enable systemd-resolved
   Created symlink /etc/systemd/system/dbus-org.freedesktop.resolve1.service → /usr/lib/systemd/system/systemd-resolved.service.
   Created symlink /etc/systemd/system/multi-user.target.wants/systemd-resolved.service → /usr/lib/systemd/system/systemd-resolved.service.
```

* Vérifier qu'un  serveur DNS tourne localement et écoute sur un port de l'interfce localhost
```bash
$ ss -lanupt | grep 53
   udp     UNCONN   0        0          127.0.0.53%lo:53            0.0.0.0:*      
   udp     UNCONN   0        0                0.0.0.0:5355          0.0.0.0:*      
   udp     UNCONN   0        0                   [::]:5355             [::]:*      
   tcp     LISTEN   0        128              0.0.0.0:5355          0.0.0.0:*      
   tcp     LISTEN   0        128        127.0.0.53%lo:53            0.0.0.0:*      
   tcp     LISTEN   0        128                 [::]:5355             [::]:*
```
On peut voir ici qu'un service écoute sur le port 53 (port par défaut du service DNS)

* 


## II Boot et Logs

* Générer un graphe de la séquence de boot

<img src="./images/graphe.svg" width="1500" height="2000">


## III Mécanismes manipulés par systemd

### 1. cgroups

*  identifier le cgroup utilisé par votre session SSH
```bash
$ ps -e -o pid,cmd,cgroup | grep ssh
    711 /usr/sbin/sshd -D -oCiphers 0::/system.slice/sshd.service
```
* identifier la RAM maximale à votre disposition
```bash
$ cat /sys/fs/cgroup/user.slice/user-1000.slice/memory.max 
   max
```

* modifier la RAM dédiée à votre session utilisateur et vérifiez le changement
```bash
$ sudo systemctl set-property user-1000.slice MemoryMax=512M
$ cat /sys/fs/cgroup/user.slice/user-1000.slice/memory.max 
   536870912
```

*  vérifier la création du fichier que systemctl set-property génère
```bash
$ cat /etc/systemd/system.control/user-1000.slice.d/50-MemoryMax.conf 
   # This is a drop-in unit file extension, created via "systemctl set-property"
   # or an equivalent operation. Do not edit.
   [Slice]
   MemoryMax=536870912
```

### 2. D-Bus
* observer, identifier, et expliquer complètement un évènement choisi

```
method call time=1575557167.512237 sender=:1.37 -> destination=org.freedesktop.Notifications serial=22 path=/org/freedesktop/Notifications; interface=org.freedesktop.Notifications; member=Notify
   string "flameshot"
   uint32 0
   string "flameshot"
   string "Info Flameshot"
   string "Capture d'écran sauvegardée sous /home/clement/Images/2019-12-05_15-46.png"
   array [
   ]
   array [
   ]
   int32 5000
```

DBus envoyé par Flameshot pour créé une Notification indiquant la sauvegarde d'une image.  
On voit que la destination est org.freedesktop.Notification.  
En suivant viennent une liste de paramêtre de la fenêtre de notification.

* envoyer un signal OU appeler une méthode
```bash
$ busctl call org.freedesktop.Notifications /org/freedesktop/Notifications org.freedesktop.Notifications Notify susssasa{sv}i Hello 0 "/home/clement/Images/AppLogo_Grafana.png" "Info Capital" "On est la pour découvrir dbus" 0 0 5000 --user
```

### 3. Restriction et isolation

* Lancer un processus sandboxé
```bash
$ sudo systemd-run --wait -t /bin/bash
   Running as unit: run-u160.service
```

* identifier le cgroup utilisé
```bash
$ systemd-cgls 
[...]
   system.slice
     ├─run-u160.service
     │ ├─2233 /bin/bash
     │ ├─2255 systemd-cgls
     │ └─2256 less
[...]
```
On sait qu'il s'agit du process run-u160.service. on voit ci dessus qu'il appartien au cgroup system.slice.  
*  lancer une nouvelle commande : ajouter des restrictions cgroups
```bash
$ sudo systemd-run -p MemoryMax=512M --wait -t /bin/bash
   Running as unit: run-u169.service
$ cat /sys/fs/cgroup/system.slice/run-u169.service/memory.max
   536870912
```

* lancer une nouvelle commande : ajouter un traçage réseau
```bash

```

* lancer une nouvelle commande : ajouter des restrictions réseau
```bash
sudo systemd-run -p IPAccounting=true -p IPAddressAllow=192.168.10.0/24 -p IPAddressDeny=any --wait -t /bin/bash
```

## IV systemd units in-depth
### 1. Exploration de services existants
* observer l'unité `auditd.service`  
path du fichier `auditd.service` : /etc/systemd/system/auditd.service  
expliquer le principe de la clause `ExecStartPost` : commande exécuté après la commande dans `ExecStart`
xpliquer les 4 "Security Settings" dans `auditd.service` :  
  * MemoryDenyWriteExecute=true : 
  * LockPersonality=true :
  * ProtectControlGroups=true :
  * ProtectKernelModules=true :

### 2. Création de service simple
```bash
$ sudo cat /etc/systemd/system/myhttp.service 
[Unit]
Description=My little web server

[Service]
Type=simple
ExecStart=python -m http.server 8080 --bind 192.168.10.11 --directory /var/www/
ExecStartPost=firewall-cmd --add-port=8080/tcp
ExecStopPost=firewall-cmd --remove-port=8080/tcp
MemoryMax=512M
```
* faites en sorte que le service se lance au démarrage
```bash
$ sudo systemctl enable myhttp
   Created symlink /etc/systemd/system/multi-user.target.wants/myhttp.service → /etc/systemd/system/myhttp.service.
```